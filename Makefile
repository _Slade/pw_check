JC       = javac
WARNINGS = -Xlint
JFLAGS   = $(WARNINGS)

RUN      = java
RUNFLAGS =

BASENAMES = pw_check Util DLBTrie Stack

FILES    = $(addprefix ./pw_check/, $(BASENAMES)) \
		   $(addprefix ./pw_check/t/, Test Tester)

MAIN     = pw_check.pw_check
TEST     = pw_check.t.Test pw_check.DLBTrie

SRC      = $(addsuffix .java,  $(FILES))

RM       = rm -vf

default : classes

classes : $(SRC)
	$(JC) $(JFLAGS) $(SRC)

debug : $(SRC)
	$(JC) $(JFLAGS) -g $(SRC)

test :
	$(foreach T, $(TEST), $(RUN) $(RUNFLAGS) $(T);)

run :
	$(RUN) $(RUNFLAGS) $(MAIN)

list-files :
	@echo $(SRC)

clean:
	$(RM) ./pw_check/*.class ./pw_check/t/*.class
