package pw_check.t;
import java.util.Objects; /* .equals()*/
/**
 * A TAP-ish testing class inspired by the Perl side of things. It isn't built
 * to take advantage of the main purpose of TAP (easy to parse and integrate
 * with automated testers), but it feels like home.
 */
public class Tester
{
    private int       passed;
    private final int planned;
    private int       numberOfTests; // Current number of tests, not planned.
    private boolean   done = false;

    // Testing without a plan
    public Tester()
    {
        planned = 0;
    }

    public Tester(final int plan)
    {
        if (plan < 1)
        {
            throw new IllegalArgumentException(
                  "You must plan at least one test"
                + "unless you use the nullary constructor"
            );
        }
        else
        {
            planned = plan;
            System.out.printf("1..%d%n", plan);
        }
    }

    /**
     * Passes a test if <code>a</code> and <code>b</code> are equal, fails
     * otherwise. Neither parameter will be modified.
     *
     * @param a
     * @param b
     */
    public boolean is(final Object a, final Object b)
    {
        return ok(Objects.equals(a, b));
    }

    /**
     * The negation of <code>Test.is()</code>. Neither parameter will be
     * modified.
     *
     * @param a
     * @param b
     */
    public boolean isnt(final Object a, final Object b)
    {
        return ok(!Objects.equals(a, b));
    }

    /**
     * Logs a test without a failure message. The boolean-returning test itself
     * is performed outside of this routine, and its result is passed as a
     * parameter. The two-parameter version is recommended over this.
     *
     * @param result The boolean result of a test, passed as a parameter.
     */
    public boolean ok(final boolean result)
    {
        return ok(result, null);
    }

    /**
     * Logs a test as with the unary <code>Test.ok()</code>, but with a string
     * that is to be outputted along with the test record in the event of a
     * failure.
     *
     * @param result The result of the test
     * @param message The message to be printed in the event of a failed test
     * @return
     */
    public boolean ok(final boolean result, final String message)
    {
        if (done)
            return false;

        StringBuilder report = new StringBuilder();
        if (!result)
            report.append("not ");
        else
            ++passed;

        report.append("ok ")
              .append(numberOfTests + 1);

        if (message != null)
            report.append(" - ")
                  .append(message);

        System.out.println(report);
        numberOfTests++;
        return result;
    }

/*
    public boolean okWithTrace(final boolean result)
    {
        return okWithTrace(result, null);
    }

    public boolean okWithTrace(final boolean result, final String message)
    {
        if (!ok(result, message))
            (new Throwable("okWithTrace")).printStackTrace(System.err);
        return result;
    }
*/

    /**
     * Marks this Test object as having finished all tests. This can be used in
     * the absence of a test plan.
     */
    public boolean doneTesting()
    {
        if (done)
            return true;
        this.done = true;

        final boolean allPassed = (numberOfTests == passed);

        if (planned == 0)
            System.out.printf("1..%d%n", numberOfTests);

        System.out.println(
            allPassed
            ? "Result: PASS"
            : "Result: FAIL"
        );
        return allPassed;
    }
}
