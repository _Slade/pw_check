package pw_check.t;
import pw_check.*;
import java.util.Objects;
public class Test
{
    public static void main(String[] args)
    {
        final Tester t = new Tester();

        SYMBOL_TEST:
        {
            boolean ok =
                   pw_check.isSymbol('!')
                && pw_check.isSymbol('@')
                && pw_check.isSymbol('$')
                && pw_check.isSymbol('%')
                && pw_check.isSymbol('&')
                && pw_check.isSymbol('*');
            t.ok(ok, "Test for false positives or negatives in isSymbol()");
        }

        IS_GOOD_WRAPPER_TEST:
        {
            {
                // boolean result = false;
                // try { pw_check.checkUserInput(null); }
                // catch (NullPointerException e) { result = true; }
                // t.ok(result, "Check for null");
            }
            // final class IsGoodTest
            // {
            //     final String message;
            //     final String arg;
            //
            //     IsGoodTest(String consMessage, String consArg)
            //     {
            //         message         = Objects.requireNonNull(consMessage);
            //         arg             = Objects.requireNonNull(consArg);
            //     }
            // }
            //
            // final IsGoodTest[] tests = {
            //     new IsGoodTest(
            //         "are too long",
            //         "toolong"
            //     ),
            //     new IsGoodTest(
            //         "are too short",
            //         "x"
            //     ),
            //     new IsGoodTest(
            //         "have too many letters",
            //         "abcde"
            //     ),
            //     new IsGoodTest(
            //         "have too many numbers",
            //         "12345"
            //     ),
            //     new IsGoodTest(
            //         "contain forbidden symbols (non-\"!@$&%*\")",
            //         "abc1^"
            //     )
            // };
            //
            // for (IsGoodTest test : tests)
            // {
            //     boolean result = false;
            //     try { pw_check.checkUserInput(test.arg); }
            //     catch (IllegalArgumentException e) { result = true; }
            //     t.ok(result, "Guard against passwords that " + test.message);
            // }
        }

        CONTAINS_CAPS:
        {
            String t1 = "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(){}[]";
            String t2 = "asdfksalcAbcdefghijklmnopqrstuvxyz";
            t.ok(
                !Util.containsCaps(t1) && Util.containsCaps(t2),
                "Util.containsCaps()"
            );
        }

        STACK:
        {
            Stack<Integer> s = new Stack<>();
            t.ok(s.isEmpty(), "Stack is empty");
            t.ok(s.pop() == null, "Empty pop == null");
            s.push(new Integer(42));
            t.ok(s.size() == 1, String.format("%d", s.size()));
            t.ok(s.peek().intValue() == 42, ".peek()");
            t.ok(s.pop().intValue() == 42, ".pop()");
            for (int i = 1; i <= 40; ++i)
                s.push(new Integer(i));
            t.ok(s.size() == 40, ".size()");
            boolean ok = true;
            for (int i = 40; i >= 1; --i)
            {
                if (s.pop().intValue() != i)
                    ok = false;
            }
            t.ok(ok, "Stack keeps things in order");
        }

        DLB_TRIE_2:
        {
            DLBTrie trie = new DLBTrie();
            String[] tests = { "foo", "bar", "baz" };
            for (int i = 0; i < tests.length; ++i)
                trie.put(tests[i]);
            t.ok(trie.contains(tests[0])
                && trie.contains(tests[1])
                && trie.contains(tests[2]), ".contains() in trie with prefixes");
        }

        t.doneTesting();
    }
}
