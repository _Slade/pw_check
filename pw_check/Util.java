package pw_check;
import java.io.UnsupportedEncodingException;
import java.io.PrintStream;
import java.util.Objects;
import java.util.Arrays;
public final class Util
{
    private Util() { }

    public static void require(boolean b, String msg)
    {
        if (!b) { throw new IllegalArgumentException(msg); }
    }

    public static void die(String msg)
    {
        System.out.println(msg);
        System.exit(1);
    }

    /**
    * Returns true or false if the given string contains an uppercase ASCII
    * letter
    *
    * @param str The string to check
    * @return A boolean
    */
    public static boolean containsCaps(String str)
    {
        Objects.requireNonNull(str);
        for (int i = 0; i < str.length(); ++i)
            if ('A' <= str.charAt(i) && str.charAt(i) <= 'Z')
                return true;
        return false;
    }

    public static Object id(Object a) { return a; }
    public static void doNothing(Object o) { }

    public static String join(Iterable<String> iter)
    {
        StringBuilder sb = new StringBuilder();
        for (String s : iter)
            sb.append(s).append(" ");
        if (sb.length() > 0)
            sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
