package pw_check;
import java.util.Objects;
import pw_check.Stack;
import pw_check.Util;
import pw_check.t.Tester;
// import java.util.function.Consumer;
// import java.util.function.Predicate;
public class DLBTrie // implements Iterable<byte[]>
{
    Node root = new Node('\0');
    int size  = 0;
    static final boolean DEBUG_CONTAINS = false;
    static final boolean DEBUG_PUT      = false;
    static final boolean DEBUG_FOREACH  = false;

    /**
    * Tests if the trie is empty.
    */
    public boolean isEmpty()
    {
        return !root.hasChild();
    }

    class Node
    {
        Node next;                // Node to the right, null by default
        Node child;               // Node to the bottom, null by default
        char val;                 // Byte value stored at this node
        boolean isEndOfWord;      // Flag if this node ends a word
        Node(char c) { val = c; } // Constructor

        boolean hasNext()  { return this.next  != null; }

        boolean hasChild() { return this.child != null; }

        public String toString()
        {
            return String.format(
                "['%c'%s%s%s]",
                val,
                isEndOfWord ? "(end)" : "",
                Objects.toString(child, ""),
                Objects.toString(next, "")
            );
        }

        public String shallowToString()
        {
            return String.format(
                "['%c'%s%s%s]",
                val,
                isEndOfWord ? "(end)" : "",
                hasChild() ? "[child]" : "",
                hasNext() ? "[next]" : ""
            );
        }

        /* Appends and returns a new node to the list this is a part of */
        private Node appendNode(char c)
        {
            Node n = this;
            while (n.hasNext())
                n = n.next;
            return n.next = new Node(c);
        }

        /* Finds and returns the first node in this list containing the char c */
        private Node findCharInRow(char c)
        {
            for (Node n = this; n != null; n = n.next)
                if (n.val == c)
                    return n;
            return null;
        }
    }

    /**
    * Retrieves the number of elements in this trie.
    */
    public int size()
    {
        return size;
    }

    /**
    * Add a string to the trie.
    *
    * @param str The string
    * @throws NullPointerException If str is null
    */
    public void put(char[] str, int offset, int length)
    {
        Objects.requireNonNull(str);
        Node node = root; // The current node we're on
        LOOP:
        for (int i = offset; i < length; ++i)
        {
            final char c = str[i];

            /* If the node has no child, the letter is
               trivially not in the next row */
            if (!node.hasChild())
            {
                node.child = new Node(c);
                node       = node.child;
                assert node != null;
                continue LOOP;
            }

            // If the child's value is «c», then we simply move down a level.
            // If the node has a child, that child may or may not be alone on
            // its level in the trie. If it is not alone, we search for the
            // byte c; otherwise, we simply create a new node on the level of
            // the child with c as its value.
            final Node child = node.child;
            if (child.val == c)
            {
                node = child;
                assert node != null;
                continue LOOP;
            }

            Node tmp = child.findCharInRow(c);
            if (tmp == null) // Not found
            {
                node = child.appendNode(c);
                assert node != null;
            }
            else
            {
                node = tmp;
                assert node != null;
            }
        }

        if (!node.isEndOfWord) // Is the same word being added twice?
        {
            node.isEndOfWord = true;
            ++size;
        }
    }

    public void put(String str)
    {
        put(str, 0, str.length());
    }

    public void put(final char[] str)
    {
        put(str, 0, str.length);
    }

    public void put(String str, final int offset, final int length)
    {
        put(str.toCharArray(), offset, length);
    }

    /**
    * Tests if this trie contains a specific string.
    *
    * @param str The string to look for
    * @param offset Where the string begins
    * @param length How long the string is
    * @return A boolean
    * @throws NullPointerException If str is null
    */
    public boolean contains(char[] str, int offset, int length)
    {
        if (length == 0)
            return true;

        if (offset + length > str.length)
            throw new IndexOutOfBoundsException(
                "offset + length cannot be larger than str.length"
            );

        if (isEmpty())
            return false;

        Node maybeLast = getEndOfPrefix(str, offset, length);
        if (maybeLast == null)
            return false;
        return maybeLast.isEndOfWord;
    }

    public boolean contains(char[] str)
    {
        return contains(str, 0, str.length);
    }

    public boolean contains(String str)
    {
        return contains(str.toCharArray(), 0, str.length());
    }

    public boolean contains(String str, int offset, int length)
    {
        return contains(str.toCharArray(), offset, length);
    }

    // This requires a Java 8 compiler
    // private void forEachNode(Consumer<Node> action)
    // {
    //     if (isEmpty())
    //         return;
    //
    //     Stack<Node> stack = new Stack<Node>();
    //     stack.push(root.child);
    //
    //     while (!stack.isEmpty())
    //     {
    //         System.err.println(stack);
    //         Node v = stack.pop();
    //         action.accept(v);
    //         if (v.hasNext())
    //             stack.push(v.next);
    //         if (v.hasChild())
    //             stack.push(v.child);
    //     }
    // }

    /**
    * Gets the node marking the end of the given prefix
    *
    * @param prefix The prefix
    * @return The node if found, otherwise null
    */
    private Node getEndOfPrefix(char[] prefix, int offset, int length)
    {
        Objects.requireNonNull(prefix);
        assert root != null;
        assert offset + length <= prefix.length;
        Node node = root;
        for (int i = offset; i < offset + length; ++i)
        {
            assert node != null;
            assert node.child != null;
            node = node.child.findCharInRow(prefix[i]);
            if (node == null)
                return null;
        }
        return node;
    }

    private Node getEndOfPrefix(char[] prefix)
    {
        assert prefix != null;
        return getEndOfPrefix(prefix, 0, prefix.length);
    }

    private Node getEndOfPrefix(String prefix)
    {
        assert prefix != null;
        return getEndOfPrefix(prefix.toCharArray(), 0, prefix.length());
    }

    private Node getEndOfPrefix(String prefix, int offset, int length)
    {
        assert prefix != null;
        return getEndOfPrefix(prefix.toCharArray(), offset, length);
    }

    /**
    * Returns a string representation of this trie
    */
    public String toString()
    {
        if (isEmpty())
            return "[]";
        return Objects.toString(root.child);
    }

    /**
    * Constructs an Iterable&lt;String&gt; of strings in this trie beginning
    * with the prefix <code>prefix</code>.
    *
    * @param prefix The prefix
    * @return The iterable
    */
    public Iterable<String> stringsWithPrefix(String prefix)
    {
        Stack<String> strings = new Stack<>();
        Node endOfPrefix      = getEndOfPrefix(prefix);

        // Prefix wasn't found in this trie, return an empty Iterable
        if (endOfPrefix == null)
            return strings;

        // Check if the prefix itself is a word and, if so, add it to our results
        if (endOfPrefix.isEndOfWord)
            strings.push(prefix);

        // If this is a leaf, there can't be any other words with this prefix
        if (!endOfPrefix.hasChild())
            return strings;

        StringBuilder string = new StringBuilder(prefix);
        // We don't want to pick up anything else on this row, so we add the
        // letters from the prefix to every string we pick up while traversing
        // and start from the child
        recSWP(string, strings, endOfPrefix.child);
        return strings;
    }

    /* Recursive DFS of the trie to gather up strings */
    private static void recSWP(StringBuilder s, Stack<String> strings, Node node)
    {
        s.append(node.val);
        if (node.isEndOfWord)
            strings.push(s.toString());

        if (node.hasChild())
            recSWP(s, strings, node.child);
        // Whenever we pop a stack frame, we pop a character off the string
        // that's being built as well.
        s.deleteCharAt(Math.max(0, s.length() - 1));

        if (node.hasNext())
            recSWP(s, strings, node.next);
    }

    /**
    * Clears this trie of all its elements.
    */
    public void clear()
    {
        if (this.isEmpty())
            return;
        root.child = null;
        size       = 0;
    }

    /**
    * Constructs an Iterable&lt;String&gt; of all strings in this trie.
    *
    * @return The Iterable of strings
    */
    public Iterable<String> strings()
    {
        return stringsWithPrefix("");
    }

    // Unit tests
    public static void main(String[] args)
    {
        Tester t = new Tester();

        // Tests can be turned on or off by toggling the comments

        // if (false)
        {
            DLBTrie trie = new DLBTrie();
            t.ok(trie.isEmpty(), "trie.isEmpty()");
            String test  = "test";
            trie.put(test);
            t.ok(trie.contains(test), "trie.contains() with 1 elem");
            String test2 = "tested";
            trie.put(test2);
            t.ok(trie.contains(test2), "trie.contains() with 2 elems");
            trie.put(test);
            t.ok(trie.size() == 2, ".size() doesn't count duplicates");
        }
        // if (false)
        {
            DLBTrie trie = new DLBTrie();
            String[] tests = { "foo", "bar", "baz" };
            for (int i = 0; i < tests.length; ++i)
                trie.put(tests[i]);
            t.ok(
                   trie.contains(tests[0])
                && trie.contains(tests[1])
                && trie.contains(tests[2]),
                ".contains() in trie with prefixes"
            );
            t.ok(trie.getEndOfPrefix("bar").val == 'r', ".endOfPrefix()");
            t.ok(!trie.contains("bat"), ".contains() checks different endings");
            // System.err.println(trie.getEndOfPrefix("b"));
        }
        if (false)
        {
            DLBTrie trie = new DLBTrie();
            String[] tests = {
                "she", "sells", "sea", "shells", "by", "the", "sea", "shore"
            };
            for (String word : tests)
                trie.put(word);

            System.err.println(Util.join(trie.strings()));
            System.err.print("'s' strings: ");
            System.err.println(Util.join(trie.stringsWithPrefix("s")));
            System.err.print("'b' strings: ");
            System.err.println(Util.join(trie.stringsWithPrefix("b")));
            System.err.print("'the': ");
            System.err.println(Util.join(trie.stringsWithPrefix("the")));
        }
        // if (false)
        {
            DLBTrie trie = new DLBTrie();
            trie.put("foot");
            trie.put("food");
            trie.put("footer");
            trie.put("footloose");
            trie.put("foodfight");
            t.ok(trie.contains("footloose", 0, 4));
            trie.clear();
            t.ok(trie.size() == 0);
            t.ok(!trie.contains("food"));
            // System.err.println(Util.join(trie.stringsWithPrefix("foot")));
        }

        // if (false)
        {
            DLBTrie trie = new DLBTrie();
            trie.put("1f");
            char[] foo = { 'a', 'a', '1', 'f', 'a' };
            t.ok(trie.contains(foo, 2, 2));
        }

        t.doneTesting();
    }
}
