package pw_check;
import java.io.*;
import pw_check.Util;
import pw_check.Stack;
public class pw_check
{
    private static final DLBTrie words   = new DLBTrie();
    private static final int PW_LEN      = 5;
    private static final char[] SOLUTION = new char[5];
    private static final String OUT_DICT = "my_dictionary.txt";
    private static final String GOOD_PASSWORDS = "good_passwords.txt";
    private static BufferedWriter PASSWORD_WRITER = null;
    private static boolean hasGoodPasswordsTxt = false;

    private static boolean GENERATE;

    private static final char SYM1 = '!',
                              SYM2 = '@',
                              SYM3 = '$',
                              SYM4 = '%',
                              SYM5 = '&',
                              SYM6 = '*';

    private static final char[] CHARS = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        SYM1, SYM2, SYM3, SYM4, SYM5, SYM6
    };

    private static final int LETTER_START = 0;
    private static final int NUMBER_START = 26;
    private static final int SYMBOL_START = 36;

    // private static int timesCalledCanExtend = 0;
    // private static int timesCalledBacktrack = 0;

    public static void main(String[] args) throws IOException
    {
        GENERATE = handleArgs(args);

        // BRACE FOR JAVA I/O
        try (
            final BufferedReader in =
                new BufferedReader(new FileReader("dictionary.txt"));
            final PrintWriter toDict =
                !GENERATE
                ? null
                : new PrintWriter(new BufferedWriter(new FileWriter(OUT_DICT)));
        )
        {
            while (in.ready())
            {
                String word = in.readLine();
                word = word.trim();
                if (word.length() > 4 || word.length() == 0 || Util.containsCaps(word))
                    continue; // This can't possibly be in a password
                words.put(word);
                if (GENERATE) { toDict.println(word); }
                for (String s : numberSubs(word))
                {
                    words.put(s);
                    if (GENERATE) { toDict.println(s); }
                }
            }
        }
        catch (FileNotFoundException e)
        {
            Util.die("Error: Could not find dictionary.txt");
        }

        // assert words.contains("a");
        // assert words.contains("4");

        File goodPasswords = new File(GOOD_PASSWORDS);
        hasGoodPasswordsTxt = goodPasswords.exists();

        if (!GENERATE)
        {
            boolean skipToGen = false;
            if (!hasGoodPasswordsTxt)
            {
                System.err.println(
                    "You don't appear to have generated good_passwords.txt yet."
                    + "\nWould you like to generate it now? If you don't"
                    + " you won't be provided with password suggestions."
                    + " [y/n]: "
                );
                skipToGen = confirmChoice();
            }
            if (!skipToGen)
            {
                displayPrompt();
                return;
            }
        }

        if (hasGoodPasswordsTxt)
        {
            System.err.printf(
                "You appear to have already generated %s."
                + " Are you sure you want to regenerate it? [y/n]: ",
                GOOD_PASSWORDS
            );
            if (System.in.read() != 'y')
                System.exit(0);
        }

        Runtime.getRuntime().addShutdownHook(
            new Thread()
            {
                @Override
                public void run()
                {
                    if (PASSWORD_WRITER != null)
                    try { PASSWORD_WRITER.close(); }
                    catch (IOException e)
                    {
                        System.err.println("Couldn't close file");
                    }
                }
            }
        );

        PASSWORD_WRITER = new BufferedWriter(
            new FileWriter(goodPasswords)
        );

        long startTime = System.currentTimeMillis();
        backtrack(0);
        long endTime   = System.currentTimeMillis();
        System.out.printf(
            "Generated good_passwords.txt in %.4f seconds.%n",
            ((double)(endTime - startTime)) / 1000.0
        );
    }

    private static boolean handleArgs(String[] args)
    {
        if (args.length > 1)
            Util.die("Error: Too many arguments");

        if (args.length == 1 && !args[0].equals("-g"))
            Util.die("Error: Argument not understood. Did you mean \"-g\"?");

        return args.length == 1 && args[0].equals("-g");
    }

    private static boolean confirmChoice()
    {
        boolean b = false;
        try { b = System.in.read() == 'y'; System.in.read(); }
        catch (IOException e)
        {
            System.err.println("Got an IOException");
            System.exit(1);
        }
        return b;
    }

    private static void displayPrompt() throws IOException
    {
        System.out.println("Enter a password. (Press CTRL-D at any time to exit)");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true)
        {
            String password = in.readLine();
            if (password == null) // CTRL-D
                System.exit(0);
            // System.err.println("Got password " + password);
            try { checkUserInput(password); }
            catch (IllegalArgumentException e)
            {
                System.out.println(
                    "Your password was invalid: " + e.getMessage()
                );
                continue;
            }

            {
                int offset = 0, length = 0;
                char[] pwd = password.toCharArray();
                {   // Keep clutter vars out of scope
                    int tmp = findWord(pwd);
                    if (tmp == 0)
                    {
                        System.out.println(
                            "This is an acceptable password. Congratulations."
                        );
                        continue;
                    }
                    offset = (tmp >> 16) & 0xFFFF;
                    length = tmp & 0xFFFF;
                }
                System.out.printf(
                    "Your password was invalid: It contained the word \"%s\"%n",
                    password.substring(offset, offset + length)
                );

                if (!hasGoodPasswordsTxt)
                    continue;

                if (offset == 0)
                {
                    System.out.println(
                        "The string you gave doesn't share any prefixes in"
                        + " common with acceptable passwords. Please try again."
                    );
                    continue;
                }
                System.out.println(
                    "Here are 10 valid passwords sharing the longest prefixes"
                    + " with that of the one you gave."
                );

                int prefixEnd = offset;
                for (int i = 1; i < length; ++i)
                    if (!words.contains(pwd, offset, i))
                        prefixEnd = offset + i;
                String prefix;
                {
                    char[] tmp = new char[prefixEnd];
                    for (int i = 0; i < prefixEnd; ++i)
                        tmp[i] = pwd[i];
                    prefix = new String(tmp);
                    System.err.println(java.util.Arrays.toString(tmp));
                }
                System.err.println("Got prefix: " + prefix);
                int printed = 0;
                for (String s : getSharedPrefixes(prefix))
                {
                    System.out.println(s);
                    ++printed;
                    if (printed >= 10)
                        break;
                }
            }
        }
    }

    private static Iterable<String> numberSubs(String s)
    {
        // There are four possibilities:
        //  1 l, 2 n, 2 s No substitutions
        //  2 l, 2 n, 1 s No substitutions
        //  2 l, 1 n, 2 s Two possible substitutions
        //  3 l, 1 n, 1 s Three possible substitutions

        final int LEN = s.length();
        assert LEN <= PW_LEN : "LEN <= PW_LEN in numberSubs()";

        Stack<String> stack = new Stack<>();
        StringBuilder sb    = new StringBuilder(s);

        for (int i = 0; i < LEN; ++i)
        {
            final char bak = sb.charAt(i);
            final char num = getNumberEquivalent(bak);
            if (num != '\0')
            {
                sb.setCharAt(i, num);
                stack.push(sb.toString());
                sb.setCharAt(i, bak);
            }
        }

        for (int i = 0; i < LEN; ++i)
        {
            final char bak = sb.charAt(i);
            final char num = getNumberEquivalent(bak);
            if (num == '\0')
                continue;

            sb.setCharAt(i, num);
            for (int j = i; j < LEN; ++j)
            {
                final char bak2 = sb.charAt(j);
                final char num2 = getNumberEquivalent(bak2);
                if (num2 != '\0')
                {
                    sb.setCharAt(j, num2);
                    stack.push(sb.toString());
                    sb.setCharAt(j, bak2);
                }
            }
            sb.setCharAt(i, bak);
        }

        return stack;
    }

    private static char nextChar(final char c)
    {
        if ('a' <= c && c < 'z')
            return (char)(c + 1);

        if (c == 'z')
            return '0';

        if ('0' <= c && c < '9')
            return (char)(c + 1);

        if (c == '9')
            return SYM1;

        switch (c)
        {
            case SYM1: return SYM2;
            case SYM2: return SYM3;
            case SYM3: return SYM4;
            case SYM4: return SYM5;
            case SYM5: return SYM6;
            case SYM6:
                System.err.println("Looping back around to 'a' at pw_check:176");
                return 'a';
            default: break;
        }
        throw new IllegalArgumentException(
            "Bad character in nextChar(): " + Character.toString(c)
        );
    }

    /**
    * A string is only permitted to have up to 3 letters, 2 numbers, or 2
    * symbols, and must have at least one of each.
    *
    * @param s The string to be checked
    * @return True if the requirements are met, false otherwise
    * @throws IllegalArgumentException If nonallowed characters are encountered
    */
    private static boolean correctLNSCounts(char[] password)
    {
        int l = 0, n = 0, s = 0;
        for (int i = 0; i < PW_LEN; ++i)
            if      (isLetter(password[i])) { ++l; }
            else if (isNumber(password[i])) { ++n; }
            else if (isSymbol(password[i])) { ++s; }

        if (l + n + s != PW_LEN)
            throw new IllegalArgumentException(
                "Password can only contain characters in a..z,"
                + " 0..9, !, @, $, %, &, *\n"
            );

        return (1 <= l && l <= 3)
            && (1 <= n && n <= 2)
            && (1 <= s && s <= 2);
    }

    private static boolean isValid(char[] password)
    {
        if (!correctLNSCounts(SOLUTION))
            return false;

        return findWord(password) == 0;
    }

    /* S = { (x₁x₂x₃x₄x₅) ∈ A₁ × A₂ × A₃ × A₄ × A₅ | P(x₁x₂x₃x₄x₅) }
     * P(x) → Q(x)
     * ∀x ∈ Aₖ. ¬Q(x₁x₂ … xₖ₋₁) → ¬Q(x₁x₂ … xₖ₋₁x)
     * Sₖ(x) = { y ∈ Aₖ | Q(xy) }
     */

    private static int letters = 0;
    private static int numbers = 0;
    private static int symbols = 0;
    // k marks which spot we're trying to place a character
    private static void backtrack(int k)
    {
        // ++timesCalledBacktrack;
        if (k == PW_LEN && isValid(SOLUTION))
        {
            try
            {
                PASSWORD_WRITER.write(SOLUTION, 0, 5);
                PASSWORD_WRITER.newLine();
            }
            catch (IOException e) { Util.die("Got an IOException"); }
        }
        else
        {
            int i = letters == 3 ? NUMBER_START
                  : numbers == 2 ? SYMBOL_START
                  : LETTER_START;
            for (; i < CHARS.length; ++i)
            {
                if (canExtend(k, CHARS[i]))
                {
                    SOLUTION[k] = CHARS[i];
                    backtrack(k + 1);
                }
            }
        }
    }

    // Can we extend SOLUTION at k with c?
    private static boolean canExtend(int k, char c)
    {
        // ++timesCalledCanExtend;
        // assert 0 <= k && k < PW_LEN;
        if (k >= PW_LEN)
            return false;

        if (c == 'a' || c == '4')
            return false;

        int l = 0, n = 0, s = 0;
        for (int i = 0; i < k; ++i)
            if      (isLetter(SOLUTION[i])) { ++l; }
            else if (isNumber(SOLUTION[i])) { ++n; }
            else                            { ++s; }

        letters = l;
        numbers = n;
        symbols = s;

        final boolean isLetter = isLetter(c);
        final boolean isNumber = isNumber(c);
        final boolean isSymbol = !(isLetter || isNumber);

        // Can't have more than three letters, two numbers, or two symbols
        if (l == 3 && isLetter
        ||  n == 2 && isNumber
        ||  s == 2 && isSymbol)
            return false;

        // We covered the one-letter words in the first pass
        if (k >= 2) // ab
            if (words.contains(SOLUTION, 0, 2))
                return false;
        if (k >= 3) // bc, abc
            if (words.contains(SOLUTION, 1, 2)
            ||  words.contains(SOLUTION, 0, 3))
                return false;
        if (k == 4) // cd, bcd, abcd
        {
            if (words.contains(SOLUTION, 2, 2)
            ||  words.contains(SOLUTION, 1, 3)
            ||  words.contains(SOLUTION, 0, 4))
                return false;
        }
        // ab bc cd de abc bcd cde abcd
        return true;
    }

    private static final int[] IDX = {
        0, 2,    1, 2,    2, 2,    3, 2,    0, 3,
        1, 3,    2, 3,    0, 4,    1, 4,    0, 5
    };

    private static int findWord(char[] password)
    {
        // Special cased for speed
        for (int i = 0; i < PW_LEN; ++i)
            if (password[i] == 'a' || password[i] == '4')
                return (i << 16) | 1;

        for (int i = 0; i < IDX.length - 1; i += 2)
        {
            final int offset = IDX[i];
            final int length = IDX[i+1];

            if (words.contains(password, offset, length))
                return (offset << 16) | (length & 0xFFFF);
                // I refuse to create a whole new class just to be able to
                // return two integers at once.

        }
        return 0; // No word found
    }

    /**
    * Takes a bad user password and searches good_passwords.txt for ones with
    * the longest shared prefix.
    *
    * @param prefix The prefix to look for matches to
    * @return An Iterable of Strings that match
    * @throws IOException If something blows up
    */
    private static Iterable<String> getSharedPrefixes(String prefix) throws IOException
    {
        // There's really no purpose for this to be implemented this way with
        // the DLBTrie, other than to show that we're able to enumerate all of
        // the strings with a certain prefix from a DLBTrie (which mine can do).

        // This could be sped up a LOT by doing a binary search on the file,
        // but:
        //    * The binary search isn't straightforward to implement due to the
        //    arithmetic involved in treating each 5-byte record plus the one or
        //    two character line ending as a single indexable element
        //    * Java's random access file API is terrible and requires manually
        //    munging bytes into strings
        DLBTrie t       = new DLBTrie();
        Stack<String> s = new Stack<>();
        try (BufferedReader in = new BufferedReader(new FileReader(GOOD_PASSWORDS)))
        {
            while (in.ready())
            {
                String pwd = in.readLine();
                t.put(pwd);
                if (t.size() >= 500)
                {
                    for (String str : t.stringsWithPrefix(prefix))
                        s.push(str);
                    t.clear();
                }
            }
        }
        catch (FileNotFoundException e) { Util.die("Got an IOException"); }
        return s;
    }

    // private static long bsearchSeek(RandomAccessFile in, String key) throws IOException
    // {
    //     long lo = 0;
    //     long hi = in.length() / 6 - 1;
    //     byte[] pwd = null;
    //     try { pwd = key.getBytes("US-ASCII"); }
    //     catch (UnsupportedEncodingException e) { }
    //     if (pwd == null)
    //         Util.die("Got an encoding exception");
    //     byte[] buf = new byte[5];
    //
    //     while (lo <= hi)
    //     {
    //         long mid = lo + (hi - lo) / 2;
    //         in.read(buf, 0, 5);
    //         in.seek(in.getFilePointer() - 5);
    //         if      (compareBytes(pwd, buf) < 0)
    //             hi = mid - 1;
    //         else if (compareBytes(pwd, buf) > 0)
    //             lo = mid + 1;
    //         else
    //             return mid;
    //         System.err.println(java.util.Arrays.toString(buf));
    //     }
    //     return -1;
    // }

    private static int compareBytes(byte[] a, byte[] b)
    {
        for (int i = 0; i < Math.min(a.length, b.length); ++i)
            if (a[i] > b[i])
                return 1;
            else if (a[i] < b[i])
                return -1;
        return 0;
    }


    /**
     * This contains a bunch of slower validation checks for user-supplied
     * passwords. By keeping it separate from the validation routine required
     * for the password generation we can hopefully keep that one faster.
     * Validates by throwing a bunch of exceptions for bad input, which the
     * caller can catch and use the messages from to handle more gracefully.
     *
     * @param password The password to be validated
     * @throws NullPointerException If password is null
     * @throws IllegalArgumentException If the password is not valid
     */
    private static void checkUserInput(String password)
    {
        if (password == null)
            throw new NullPointerException("Password cannot be null");

        Util.require(password.length() == PW_LEN, "Password must be 5 characters");

        // Caller has to catch this
        Util.require(
            correctLNSCounts(password.toCharArray()),
              "Password must contain 1-3 lowercase letters, 1-2"
            + " numbers, or 1-2 symbols, totaling 5."
        );
    }

    /** Checks if a character is a lowercase ASCII letter
    * @param c The character to be checked
    * @return True or false if it's a letter or not
    */
    public static boolean isLetter(char c)
    {
        return 'a' <= c && c <= 'z';
    }


    /** Checks if a character is one of the approved symbols
    * @param c The character to be checked
    * @return True or false if it's an approved symbol or not
    */
    public static boolean isSymbol(char c)
    {
        return c == '!'
            || c == '@'
            || c == '$'
            || c == '%'
            || c == '&'
            || c == '*';
    }

    /** Checks if a character is an ASCII digit
    * @param c The character to be checked
    * @return True or false if it's a digit or not
    */
    public static boolean isNumber(char c)
    {
        return '0' <= c && c <= '9';
    }

    /**
    * Gets the equivalent number for a character, if it exists. Note that this
    * function is <b>not</b> injective and cannot be inverted.
    *
    * @param c The character to get the number equivalent of
    * @return The number equivalent, or '\0' (ASCII NUL) if there isn't one.
    */
    private static char getNumberEquivalent(char c)
    {
        switch (c)
        {
            case 't': return '7';
            case 'a': return '4';
            case 'o': return '0';
            case 'e': return '3';
            case 'i': return '1';
            case 'l': return '1';
            case 's': return '5';
            default: return '\0';
        }
    }
}
