package pw_check;
import java.util.Arrays;
import java.util.Iterator;
/**
* A weakly-typed stack for shoving stuff, mostly for the trie traversal.
*/
public class Stack<T> implements Iterable<T>
{
    private Object[] buf;
    private int bufcnt = 0;

    public Stack()
    {
        buf = new Object[32];
    }

    public Stack(int initialSize)
    {
        buf = new Object[initialSize];
    }

    @SuppressWarnings("unchecked")
    public T pop()
    {
        if (bufcnt == 0)
            return null;
        return (T) buf[--bufcnt];
    }

    @SuppressWarnings("unchecked")
    public T peek()
    {
        return (T) buf[bufcnt - 1];
    }

    public boolean isEmpty()
    {
        return bufcnt == 0;
    }

    public void push(T c)
    {
        if (bufcnt >= buf.length)
            grow();
        buf[bufcnt++] = c;
    }

    private void grow()
    {
        @SuppressWarnings("unchecked")
        T[] tmp = (T[]) new Object[buf.length * 2];
        System.arraycopy(buf, 0, tmp, 0, bufcnt);
        buf = tmp;
    }

    private void trim()
    {
        @SuppressWarnings("unchecked")
        T[] tmp = (T[]) new Object[bufcnt];
        System.arraycopy(buf, 0, tmp, 0, bufcnt);
        buf = tmp;
    }

    public int size()
    {
        return bufcnt;
    }

    public T[] toArray()
    {
        @SuppressWarnings("unchecked")
        T[] copy = (T[]) new Object[bufcnt];
        System.arraycopy(buf, 0, copy, 0, bufcnt);
        return copy;
    }

    public Iterator<T> iterator()
    {
        return Arrays.asList(this.toArray()).iterator();
    }

    public String toString()
    {
        StringBuilder bldr = new StringBuilder("[");
        for (int i = 0; i < bufcnt; ++i)
        {
            bldr.append(buf[i] == null ? "null" : buf[i].toString())
                .append(", ");
        }
        return bldr.append(buf[bufcnt]).append("]").toString();
    }
}
