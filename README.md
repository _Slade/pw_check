# pw_check

Enumerates possible good passwords (passwords not containing common words or
common words with letters substituted for numbers) and checks if entered
passwords are good or not.

Uses a custom De la Brandias trie (prefix-path data structure) and recursive
backtracking to achieve fairly good performance.
